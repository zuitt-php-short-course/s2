<?php 
require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale-1">
	<title>PHP SC S02 A01</title>
</head>
<body>
	
	<h3>Divisibles of Five</h3>
	<?php forLoop(); ?>

    <h3>Array Manipulation</h3>

    <?php $students = array(); ?>

    <?php array_push($students, 'John Smith'); ?>

	<pre><?php print_r($students); ?></pre>

    <p><?= count($students); ?></pre>	

    <?php array_push($students, 'Jane Smith'); ?>
    <pre><?php print_r($students); ?></pre>
    <p><?= count($students); ?></pre>

    <?php array_shift($students); ?>
	<pre><?php print_r($students); ?></pre>
    <p><?= count($students); ?></pre>

</body>
</html>
